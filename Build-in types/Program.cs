﻿using System;
using System.Globalization;

namespace Build_in_types
{
    class Program
    {
        static void Main(string[] args)
        {
            #region  integral types

            //sbyte _sbyte = -12;
            //Console.WriteLine($"sbyte: {_sbyte}");
            //Console.WriteLine($"value range of sbyte is {sbyte.MinValue} to {sbyte.MaxValue}");
            //Console.WriteLine($"one sbyte variable takes {sizeof(sbyte)} bytes of memory  \n\r");

            //byte _byte = 255;
            //Console.WriteLine($"byte: {_byte}");
            //Console.WriteLine($"value range of byte is {byte.MinValue} to {byte.MaxValue}");
            //Console.WriteLine($"one byte variable takes {sizeof(byte)} bytes of memory  \n\r");

            //short _short = -30000;
            //Console.WriteLine($"short: {_short}");
            //Console.WriteLine($"value range of short is {short.MinValue} to {short.MaxValue}");
            //Console.WriteLine($"one short variable takes {sizeof(short)} bytes of memory  \n\r");

            //ushort _ushort = 60000;
            //Console.WriteLine($"ushort: {_ushort}");
            //Console.WriteLine($"value range of ushort is {ushort.MinValue} to {ushort.MaxValue}");
            //Console.WriteLine($"one ushort variable takes {sizeof(ushort)} bytes of memory  \n\r");

            //int _int = -2000000000;
            //Console.WriteLine($"int: {_int}");
            //Console.WriteLine($"value range of int is {int.MinValue} to {int.MaxValue}");
            //Console.WriteLine($"one int variable takes {sizeof(int)} bytes of memory  \n\r");

            //uint _uint = 4000000000;
            //Console.WriteLine($"uint: {_uint}");
            //Console.WriteLine($"value range of uint is {uint.MinValue} to {uint.MaxValue}");
            //Console.WriteLine($"one uint variable takes {sizeof(uint)} bytes of memory  \n\r");

            //long _long = -9000000000000000000;
            //Console.WriteLine($"long: {_long}");
            //Console.WriteLine($"value range of long is {long.MinValue} to {long.MaxValue}");
            //Console.WriteLine($"one long variable takes {sizeof(long)} bytes of memory  \n\r");

            //ulong _ulong = 18000000000000000000;
            //Console.WriteLine($"ulong: {_ulong}");
            //Console.WriteLine($"value range of ulong is {ulong.MinValue} to {ulong.MaxValue}");
            //Console.WriteLine($"one ulong variable takes {sizeof(ulong)} bytes of memory  \n\r");

            //char _char1 = 'X';        // Character literal
            //char _char2 = '\x0058';   // Hexadecimal
            //char _char3 = (char)88;   // Cast from integral type
            //char _char4 = '\u0058';   // Unicode
            //Console.WriteLine($"{_char1}, {_char2}, {_char3}, {_char4}");
            //Console.WriteLine((int)_char1);

            //Console.ReadKey();

            #endregion

            #region floating-point types

            //float _float = 0.999999f;
            //Console.WriteLine($"float: {_float}");
            //Console.WriteLine($"value range of float is {float.MinValue} to {float.MaxValue}");
            //Console.WriteLine($"one float variable takes {sizeof(float)} bytes of memory  \n\r");

            //double _double = 0.999999999999999d;
            //Console.WriteLine($"double: {_double}");
            //Console.WriteLine($"value range of double is {double.MinValue} to {double.MaxValue}");
            //Console.WriteLine($"one double variable takes {sizeof(double)} bytes of memory  \n\r");

            //decimal _decimal = 0.9999999999999999999999999999m;
            //Console.WriteLine($"decimal: {_decimal}");
            //Console.WriteLine($"value range of decimal is {decimal.MinValue} to {decimal.MaxValue}");
            //Console.WriteLine($"one decimal variable takes {sizeof(decimal)} bytes of memory  \n\r");

            //Console.ReadKey();

            #endregion

            #region bool

            //bool value = true;
            //bool negatedValue = !value;
            //Console.WriteLine($"initial value : {value}");
            //Console.WriteLine($"negated value : {negatedValue}");

            //bool and = value && negatedValue;
            //Console.WriteLine($"{value} and {negatedValue} gives {and}");
            //bool or = value || negatedValue;
            //Console.WriteLine($"{value} or {negatedValue} gives {or}");
            //Console.ReadKey();

            #endregion

            #region string

            string hello = "Hello";
            string world = "world";

            string sum = hello + " " + world;
            bool identical = hello == world;
            bool equalResult = hello.Equals(world);
            int compareResult = hello.CompareTo(world);
            bool staticEqual = string.Equals(hello, world);
            int staticCompare = string.Compare(hello, world);

            bool contains = hello.Contains("llo");
            string lowerCase = hello.ToLower();
            string upperCase = hello.ToUpper();
            int index = hello.IndexOf("l");

            char secondChar = hello[1];

            Console.ReadKey();

            #endregion
        }
    }
}
