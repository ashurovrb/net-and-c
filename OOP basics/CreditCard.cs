﻿using System;

namespace OOP_basics
{
    struct CreditCard : IEquatable<CreditCard>
    {
        public int BankAccountId { get; set; }

        public static bool operator ==(CreditCard first, CreditCard second)
        {
            if (first.BankAccountId == second.BankAccountId) return true;
            else return false;
        }

        public static bool operator !=(CreditCard first, CreditCard second)
        {
            if (first.BankAccountId != second.BankAccountId) return true;
            else return false;
        }

        public override int GetHashCode()
        {
            return BankAccountId.GetHashCode();
        }

        public bool Equals(CreditCard other)
        {
            return BankAccountId == other.BankAccountId;
        }

        public override bool Equals(object obj)
        {
            return obj is CreditCard && Equals((CreditCard)obj);
        }
    }
}
