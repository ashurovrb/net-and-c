﻿using System;

namespace OOP_basics
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                // instantiation of new Wallet instance
                var wallet = new Wallet(15);
                // method execution
                wallet.AddMoney(15);

                // indexer usage
                var card = wallet[1];
                wallet[0] = new CreditCard();

                // delegate usage
                wallet.SendMoneyToUI = SendMoneyAmountToConsole;
                wallet.SendMoneyToUI(wallet.Money);
                // event usage
                wallet.SendMoneyToUIEvent += SendMoneyAmountToConsole;
                wallet.InvokeSendMoneyToUIEvent();
            }

            {
                // value types instances identity
                var card1 = new CreditCard() { BankAccountId = 111 };
                var card2 = new CreditCard() { BankAccountId = 111 };
                Console.WriteLine(card1 == card2);

                // reference types instances identity
                var wallet1 = new Wallet(15);
                var wallet2 = new Wallet(15);
                Console.WriteLine(wallet1 == wallet2);
            }

            Console.ReadKey();
        }

        static void SendMoneyAmountToConsole(int amount)
        {
            Console.WriteLine("Amount of money on wallet is " + amount);
        }
    }



    
}
