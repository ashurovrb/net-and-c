﻿namespace OOP_basics
{
    class Wallet
    {
        public SendMoneyToUIHandler SendMoneyToUI;
        public event SendMoneyToUIHandler SendMoneyToUIEvent;
        private int money = 15;
        private CreditCard[] cards;

        public int Money
        {
            get { return money; }
            set
            {
                if (value > 0) money = value;
            }
        }

        public Wallet()
        {
            this.cards = new CreditCard[3] {
                new CreditCard
                {
                    BankAccountId = 123
                },
                new CreditCard()
                {
                    BankAccountId = 777
                },
                new CreditCard()
                {
                    BankAccountId = 333
                }
            };
        }

        public Wallet(int moneyAmount) : this()
        {
            if (moneyAmount < 0) return;
            else this.money = moneyAmount;
            // or if (moneyAmount > 0) this.money = moneyAmount;
        }

        public void AddMoney(int amount)
        {
            this.money += amount;
        }

        public void InvokeSendMoneyToUIEvent()
        {
            this.SendMoneyToUIEvent?.Invoke(this.money);
        }

        public CreditCard this[int index]
        {
            get { return this.cards[index]; }
            set { this.cards[index] = value; }
        }

        ~Wallet()
        {
            this.money = 0;
        }
    }
}
