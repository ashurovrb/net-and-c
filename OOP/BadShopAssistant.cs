﻿namespace OOP
{
    class BadShopAssistant : User, IShopAssistant
    {
        private int Overcharge { get; } = 10;
        public BadShopAssistant(string nickname, string realname, int age) : base(nickname, realname, age)
        {
        }

        public OrderProcessingResult ProcessCustomerOrder(ICustomer customer)
        {
            if (customer.OrderItems.Count > 0)
            {
                int totalCosts = Overcharge;

                foreach (OrderItem item in customer.OrderItems)
                {
                    totalCosts += item.Price;
                }

                var transactionResult = customer.MakePayment(totalCosts);
                if (transactionResult)
                {
                    customer.OrderItems.Clear();
                    return new OrderProcessingResult { IsSuccess = true, Amount = totalCosts -10 };
                }
                else
                {
                    return new OrderProcessingResult { IsSuccess = false, Message = "Payment transaction failed" };
                }
            }
            return new OrderProcessingResult { IsSuccess = false, Message = "No order items to process" };
        }
    }
}
