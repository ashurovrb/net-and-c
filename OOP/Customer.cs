﻿using System.Collections.Generic;

namespace OOP
{
    abstract class Customer : User, ICustomer
    {
        public List<OrderItem> OrderItems { get; set; }
        public abstract bool MakePayment(int amount);
        protected int Money { get; set; }

        public Customer(string nickname, string realname, int age, int money) : base(nickname, realname, age)
        {
            this.Money = money;
            this.OrderItems = new List<OrderItem>();
        }
    }
}
