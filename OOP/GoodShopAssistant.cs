﻿namespace OOP
{
    class GoodShopAssistant : User, IShopAssistant
    {
        public GoodShopAssistant(string nickname, string realname, int age) : base(nickname, realname, age)
        {
        }

        public OrderProcessingResult ProcessCustomerOrder(ICustomer customer)
        {
            if (customer.OrderItems.Count > 0)
            {
                int totalCosts = 0;

                foreach (OrderItem item in customer.OrderItems)
                {
                    totalCosts += item.Price;
                }

                var transactionResult = customer.MakePayment(totalCosts);
                if (transactionResult)
                {
                    customer.OrderItems.Clear();
                    return new OrderProcessingResult { IsSuccess = true, Amount = totalCosts };
                }
                else
                {
                    return new OrderProcessingResult { IsSuccess = false, Message = "Payment transaction failed" };
                }
            }
            return new OrderProcessingResult { IsSuccess = false, Message = "No order items to process" };
        }
    }
}
