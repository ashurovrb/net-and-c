﻿using System.Collections.Generic;

namespace OOP
{
    interface ICustomer
    {
        List<OrderItem> OrderItems { get; }
        bool MakePayment(int amount);
    }
}
