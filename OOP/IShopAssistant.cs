﻿namespace OOP
{
    interface IShopAssistant
    {
        OrderProcessingResult ProcessCustomerOrder(ICustomer customer);
    }
}
