﻿namespace OOP
{
    class NormalCustomer : Customer
    {
        public NormalCustomer(string nickname, string realname, int age, int money) : base(nickname, realname, age, money)
        {
        }

        public override bool MakePayment(int amount)
        {
            if (this.Money - amount >= 0)
            {
                this.Money = this.Money - amount;
                return true;
            }
            else return false;
        }
    }
}
