﻿namespace OOP
{
    class OrderItem
    {
        public OrderItemType Type { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
    }
}
