﻿namespace OOP
{
    class OrderProcessingResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public int Amount { get; set; }
    }
}
