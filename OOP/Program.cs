﻿using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            IShopAssistant mickey = new GoodShopAssistant("Mickey", "Mouse", 70);
            IShopAssistant goofy = new BadShopAssistant("Goofy", "Goof", 30);

            ICustomer donald = new NormalCustomer("Donald", "Duck", 50, new Random().Next(50));
            ICustomer scrooge = new VipCustomer("Scrooge", "Mcduck", 50, new Random().Next(100));

            donald.OrderItems.Add(new OrderItem
            {
                Type = OrderItemType.Food,
                Description = "Cheese",
                Price = 10
            });
            donald.OrderItems.Add(new OrderItem
            {
                Type = OrderItemType.Toys,
                Description = "Red Dead Redemption 2",
                Price = 10
            });

            scrooge.OrderItems.Add(new OrderItem
            {
                Type = OrderItemType.Food,
                Description = "Fish",
                Price = 30
            });
            scrooge.OrderItems.Add(new OrderItem
            {
                Type = OrderItemType.Electronics,
                Description = "Nintendo switch",
                Price = 200
            });

            var mickeyResult = mickey.ProcessCustomerOrder(donald);
            var goofyResult = goofy.ProcessCustomerOrder(scrooge);

            if (mickeyResult.IsSuccess)
            {
                Console.WriteLine($"Mickey earned {mickeyResult.Amount}");
            }
            else
            {
                Console.WriteLine($"Mickey was unable to process order because {mickeyResult.Message}");
            }

            if (goofyResult.IsSuccess)
            {
                Console.WriteLine($"Goofy earned {goofyResult.Amount}");
            }
            else
            {
                Console.WriteLine($"Goofy was unable to process order because {goofyResult.Message}");
            }

            Console.ReadKey();
        }
    }
}
