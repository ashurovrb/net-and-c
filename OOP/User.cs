﻿namespace OOP
{
    abstract class User
    {
        public string FirstName { get; }
        public string SecondName { get; }
        public int Age { get; }

        public User(string firstName, string secondName, int age)
        {
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.Age = age;
        }
    }
}
