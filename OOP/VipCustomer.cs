﻿namespace OOP
{
    class VipCustomer : Customer
    {
        private int TotalamountOfCredit;
        public VipCustomer(string nickname, string realname, int age, int money) : base(nickname, realname, age, money)
        {

        }

        public override bool MakePayment(int amount)
        {
            if(this.Money - amount >= 0)
            {
                this.Money = this.Money - amount;
                return true;
            }
            else
            {
                bool bankDecision = Bank.GiveCredit(amount, $"{this.FirstName} {this.SecondName}");
                if (bankDecision)
                {
                    this.TotalamountOfCredit = this.TotalamountOfCredit + amount;
                    return true;
                }
                else return false;
            }
        }
    }
}
