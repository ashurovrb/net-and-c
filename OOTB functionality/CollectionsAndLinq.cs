﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace OOTB_functionality
{
    //[DataContract]
    public class Product
    {
        //[DataMember]
        [XmlAttribute("Value")]
        public int Amount { get; set; }
        //[XmlIgnore]
        [XmlElement("Description")]
        public string Name { get; set; }
        //[DataMember]
        private string Hidden { get; set; } = "HiddenValue";
    }

    public class Order
    {
        public List<Product> Products { get; set; }
        public void AddProduct(Product product)
        {
            if (product == null) throw new ArgumentNullException(nameof(product));
            this.Products.Add(product);
        }
    }
}
