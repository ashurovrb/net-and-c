﻿namespace OOTB_functionality
{
    abstract class Animal
    {
        public bool IsHungry { get; set; }
        public void Eat()
        {
            this.IsHungry = false;
        }
    }

    class Dog : Animal
    {

    }

    class Cat : Animal
    {

    }

    class Kennel<T> where T : Animal
    {
        public T pet { get; set; }
        public void FeedThePet()
        {
            this.pet.Eat();
        }
    }

    static class DefaultValueFactory
    {
        public static T GetDefaultValue<T>()
        {
            return default(T);
        }

        public static T GetDefaultValue<T>(T variableOfType)
        {
            return default(T);
        }
    }

    interface IStupidPlaceSwither<T> where T: struct
    {
        (T,T) GenerateTupple(T value1, T value2);
    }

    class StupidPlaceSwither<T> : IStupidPlaceSwither<T> where T : struct
    {
        public (T, T) GenerateTupple(T value1, T value2)
        {
            return (value2, value1);
        }
    }
}
