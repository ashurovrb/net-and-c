﻿using System;
using System.Globalization;

namespace OOTB_functionality
{
    struct Temperature : ICloneable, IEquatable<Temperature>, IComparable, IComparable<Temperature>, IDisposable, IFormattable
    {
        private readonly decimal temp;

        public Temperature(decimal temp)
        {
            this.temp = temp;
        }
        public object Clone()
        {
            return new Temperature(this.temp);
        }

        public bool Equals(Temperature other)
        {
            return this.temp == other.temp;
        }

        public int CompareTo(object other)
        {
            if (other == null)
                return 1;
            if (!(other is Temperature))
                throw new ArgumentException("argument is not Temperature instance");
            return this.CompareTo((Temperature)other);
        }

        public int CompareTo(Temperature other)
        {
            return this.temp.CompareTo(other.temp);
        }

        public void Dispose()
        {
            // here we can release unmanaged resources like file handles, db connections, etc.
            Console.WriteLine("Method Dispose was called");
        }

        public string ToString(string format, IFormatProvider provider)
        {
            if (string.IsNullOrEmpty(format)) format = "G";
            if (provider == null) provider = CultureInfo.CurrentCulture;

            switch (format.ToUpperInvariant())
            {
                case "G":
                case "C":
                    return temp.ToString("F2", provider) + " °C";
                case "F":
                    return (temp * 9 / 5 + 32).ToString("F2", provider) + " °F";
                case "K":
                    return (temp + 273.15m).ToString("F2", provider) + " K";
                default:
                    throw new FormatException(string.Format("The {0} format string is not supported.", format));
            }
        }
    }
}
