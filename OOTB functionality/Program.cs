﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml.Serialization;

namespace OOTB_functionality
{
    class Program
    {
        static void Main(string[] args)
        {
            // Generics
            {
                #region Generic classes

                //var dogKennel = new Kennel<Dog>();
                //dogKennel.pet = new Dog();
                //dogKennel.FeedThePet();
                //Console.WriteLine(dogKennel.GetType());

                //var catKennel = new Kennel<Cat>();
                //catKennel.pet = new Cat();
                //catKennel.FeedThePet();
                //Console.WriteLine(catKennel.GetType());

                //Console.ReadKey();

                #endregion

                #region Generic methods

                //var defBoolVal = DefaultValueFactory.GetDefaultValue<bool>();

                //var val = 5;
                //var defIntVal = DefaultValueFactory.GetDefaultValue(val);

                //Console.WriteLine($"Default value of bool is: {defBoolVal}");
                //Console.WriteLine($"Default value of int is: {defIntVal}");
                //Console.ReadKey();

                #endregion

                #region Generic interfaces

                //int x = 3;
                //int y = 7;
                //(x, y) = new StupidPlaceSwither<int>().GenerateTupple(x, y);

                //Console.WriteLine($"x = {x}, y = {y}");
                //Console.ReadKey();

                #endregion
            }

            #region Important interfaces

            //var normalTemp = new Temperature(36.6m);
            //var fluTemp = new Temperature(39m);

            //bool isEqual = normalTemp.Equals(fluTemp);

            //var cloned = (Temperature)normalTemp.Clone();
            //bool isCloneEqual = normalTemp.Equals(cloned);

            //int orderValue = normalTemp.CompareTo(fluTemp);

            //Console.WriteLine($"{normalTemp.ToString("G", CultureInfo.CurrentCulture)} and {fluTemp.ToString("G", CultureInfo.CurrentCulture)} is equal: {isEqual}");
            //Console.WriteLine($"{normalTemp.ToString("F", CultureInfo.CurrentCulture)} and {cloned.ToString("F", CultureInfo.CurrentCulture)} is equal: {isCloneEqual}");

            //using (normalTemp)
            //{
            //    Console.WriteLine("Example of using construction");
            //}

            //Console.ReadLine();

            #endregion

            // Collections
            {
                #region List<T>

                //List<string> salmons = new List<string>();
                //salmons.Add("chinook");
                //salmons.Add("coho");
                //salmons.Add("pink");
                //salmons.Add("sockeye");

                //bool result = salmons.Remove("coho");
                //if (result) Console.WriteLine("coho was deleted \r\n");

                //for (var index = 0; index < salmons.Count; index++)
                //{
                //    Console.Write(salmons[index] + "\r\n");
                //}
                //Console.WriteLine();

                //// collection initializer.  
                //var cats = new List<string> { "Snowflake", "Mars", "Kitty", "Spikey" };

                //// Iterate through the list.  
                //foreach (var cat in cats)
                //{
                //    Console.Write($"{cat} is the {cats.IndexOf(cat)}th element in the collection \r\n");
                //}
                //Console.WriteLine();

                //cats.Sort();
                //cats.ForEach(cat => Console.Write($"{cat} is the {cats.IndexOf(cat)}th element in the collection \r\n"));

                //Console.ReadKey();

                #endregion

                #region Dictionary<TKey, TValue>

                //Dictionary<int, string> fishes = new Dictionary<int, string>();
                //fishes.Add(2, "chinook");
                //fishes.Add(4, "coho");
                //fishes.Add(1, "pink");
                //fishes.Add(5, "sockeye");

                //bool result = fishes.Remove(4);
                //if (result) Console.WriteLine("coho was deleted \r\n");

                //string coho;
                ////coho = fishes[4];
                //if (!fishes.ContainsKey(4))
                //{
                //    fishes.Add(4, "coho");
                //    //fishes.Add(4, "coho");
                //}
                //coho = fishes[4];

                //foreach (var kvp in fishes)
                //{
                //    Console.Write($"{kvp.Value} has key value {kvp.Key} in the dictionary \r\n");
                //}
                //Console.WriteLine();

                //Console.ReadKey();

                #endregion

                #region HashSet<T>

                //HashSet<string> salmons = new HashSet<string>();
                //salmons.Add("chinook");
                //salmons.Add("coho");
                //salmons.Add("pink");
                //salmons.Add("sockeye");
                //salmons.Add("sockeye");
                //salmons.Add("sockeye");

                //HashSet<string> fishes = new HashSet<string>();
                //fishes.Add("chinook");
                //fishes.Add("coho");
                //fishes.Add("red");


                //salmons.IntersectWith(fishes);
                //// Iterate through the list.  
                //foreach (var fish in salmons)
                //{
                //    Console.Write($"{fish} \r\n");
                //}
                //Console.WriteLine();

                //bool isSuperset = fishes.IsSupersetOf(salmons);
                //Console.WriteLine($"Fishes is superset of salmons: {isSuperset}");

                //Console.ReadKey();

                #endregion
            }

            // LINQ (LINQ to objects)
            {
                #region Query syntax

                //int[] data = { 1, 2, 5, 8, 4 };
                //var allBiggerThan3 = from d in data
                //                     where d > 3
                //                     select d;
                //Console.WriteLine(string.Join(",", allBiggerThan3));
                //Console.WriteLine();

                //List<Order> orders = new List<Order>
                //{
                //    new Order
                //    {
                //        Products = new List<Product>
                //        {
                //            new Product { Amount = 6, Name = "Camera" },
                //            new Product { Amount = 1, Name = "Pizza" },
                //        }
                //},
                //    new Order
                //    {
                //        Products = new List<Product>
                //        {
                //            new Product { Amount = 3, Name = "TvGame" },
                //            new Product { Amount = 5, Name = "TV" },
                //        }
                //    },
                //};

                //var result = from o in orders
                //             from l in o.Products
                //             orderby l.Amount descending
                //             select new
                //             {
                //                 Name = l.Name,
                //                 Amount = l.Amount
                //             };

                //foreach (var res in result)
                //{
                //    Console.WriteLine($"{res.Name} {res.Amount}");
                //}

                //Console.ReadKey();

                #endregion

                #region Query syntax


                //int[] data = { 1, 2, 5, 8, 4 };
                //var allBiggerThan3 = data.Where(elem => elem > 3).ToArray();

                //Console.WriteLine(string.Join(",", allBiggerThan3));
                //Console.WriteLine();

                //List<Order> orders = new List<Order>
                //{
                //    new Order
                //    {
                //        Products = new List<Product>
                //        {
                //            new Product { Amount = 6, Name = "Camera" },
                //            new Product { Amount = 1, Name = "Pizza" },
                //        }
                //},
                //    new Order
                //    {
                //        Products = new List<Product>
                //        {
                //            new Product { Amount = 3, Name = "TvGame" },
                //            new Product { Amount = 5, Name = "TV" },
                //        }
                //    },
                //};

                //var result = orders
                //    .SelectMany(o => o.Products)
                //    .OrderByDescending(p => p.Amount)
                //    .Select(p => new { p.Name, p.Amount });

                //foreach (var res in result)
                //{
                //    Console.WriteLine($"{res.Name} {res.Amount}");
                //}

                //Console.ReadKey();

                #endregion
            }

            #region Exception handling 

            //var order = new Order();

            //try
            //{
            //    order.AddProduct(null);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine($"Message: '{e.Message}' from {e.Source}");
            //}
            //finally
            //{
            //    Console.WriteLine("Yes we can!");
            //}

            //Console.ReadKey();

            #endregion

            #region Math

            //const double pi = Math.PI;
            //double absValue = Math.Abs(-12.4);
            //double maxRoundVal = Math.Floor(2.56);
            //double roundVal = Math.Round(20.77);
            //int sign = Math.Sign(15);
            //int anotherSign = Math.Sign(-5);
            //double squareRoot = Math.Sqrt(16);

            //Console.WriteLine($"PI constant = {pi}");
            //Console.WriteLine($"Absolute value of -12.4 = {absValue}");
            //Console.WriteLine($"Maximum round value of 2.56 = {maxRoundVal}");
            //Console.WriteLine($"Round value of 20.77 = {roundVal}");
            //Console.WriteLine($"Sign of 15 = {sign}");
            //Console.WriteLine($"Sign of -5 = {anotherSign}");
            //Console.WriteLine($"{squareRoot} * {squareRoot} = 16");
            //Console.ReadKey();

            #endregion

            #region Nullable value types

            //Nullable<int> x = 5;
            //x = null;
            //Console.WriteLine($"x has value: {x.HasValue}");

            //bool? y = true;
            //Console.WriteLine($"y has value: {y.HasValue}, value is: {y.Value}");

            #endregion

            // Date and Time
            {
                #region DateTime and TimeSpan

                //DateTime current = DateTime.Now;
                //DateTime someDateTime = new DateTime(1989, 6, 6);

                //// The value of this property represents the number of 100-nanosecond intervals that have elapsed since
                //// 12:00:00 midnight, January 1, 0001 (0:00:00 UTC on January 1, 0001, in the Gregorian calendar)
                //long ticks = current.Ticks;

                //Console.WriteLine($"Current local date is {current}");
                //Console.WriteLine($"This value is equal to {ticks} ticks \r\n");

                //Console.WriteLine($"Current date contain properties: \r\n" +
                //    $"Only date information: {current.Date} \r\n" +
                //    $"Only time information: {current.TimeOfDay} \r\n" +
                //    $"This day is {current.DayOfYear} day of the year \r\n" +
                //    $"{current.Hour}:{current.Minute}:{current.Second}:{current.Millisecond} \r\n" +
                //    $"It is {current.Kind} time \r\n" +
                //    $"{current.DayOfWeek} {current.Day}/{current.Month}/{current.Year} \r\n");
                //Console.WriteLine(current + "\r\n");

                //Console.WriteLine(current.ToShortTimeString());
                //Console.WriteLine(current.ToLongTimeString());
                //Console.WriteLine(current.ToLongDateString());
                //Console.WriteLine(current.ToShortDateString() + "\r\n");

                //Console.WriteLine(current.ToString("F", CultureInfo.InvariantCulture));
                //Console.WriteLine(current.ToString(new CultureInfo("fr-FR")));
                //Console.WriteLine(current.ToString("F", new CultureInfo("fr-FR")));
                //Console.WriteLine();

                //TimeSpan time = current.TimeOfDay;
                //Console.WriteLine($"Current time is {time}");

                //DateTime currentParsedFromLocalFormat = DateTime.Parse(current.ToString());
                //DateTime currentParsedFromFrFormat = DateTime.Parse(current.ToString("F", new CultureInfo("fr-FR")), new CultureInfo("fr-FR"));

                //DateTime currentUtc = DateTime.UtcNow;
                //Console.WriteLine($"Current {currentUtc.Kind} date is {currentUtc} \r\n");
                //Console.WriteLine($"Current local date to utc date is {current.ToUniversalTime()}");
                //Console.WriteLine($"Current utc date to local date is {currentUtc.ToLocalTime()} \r\n");

                //TimeZoneInfo localTimeZone = TimeZoneInfo.Local;
                //Console.WriteLine($"Local timezone is {localTimeZone}");
                //Console.WriteLine($"It has daylight saving time rules" +
                //    $": {localTimeZone.SupportsDaylightSavingTime} \r\n");

                //TimeZoneInfo hawaiianZone = TimeZoneInfo.FindSystemTimeZoneById("Hawaiian Standard Time");
                //Console.WriteLine($"{current.Kind} time to Havaiian time zone: {TimeZoneInfo.ConvertTime(current, hawaiianZone)}");
                //Console.WriteLine($"{currentUtc.Kind} time to Havaiian time zone: {TimeZoneInfo.ConvertTime(currentUtc, hawaiianZone)}");
                //Console.ReadKey();

                #endregion

                #region DateTimeOffset

                //DateTimeOffset dateOffset = DateTimeOffset.Now;
                //TimeSpan offset = dateOffset.Offset;

                //Console.WriteLine(dateOffset);
                //Console.WriteLine(offset);
                //Console.ReadKey();

                //// Although a DateTimeOffset value includes an offset, it is not a fully time zone - aware data structure.
                //// While an offset from UTC is one characteristic of a time zone, it does not unambiguously identify a time zone.
                //// Not only do multiple time zones share the same offset from UTC, but the offset of a single time zone changes if it observes daylight saving time.
                //// This means that, as soon as a DateTimeOffset value is disassociated from its time zone, it can no longer be unambiguously linked back to its original time zone.

                #endregion
            }

            // I/O functionality
            {
                #region Drives

                //DriveInfo[] drivesInfo = DriveInfo.GetDrives();

                //foreach (DriveInfo driveInfo in drivesInfo)
                //{
                //    Console.WriteLine("Drive {0}", driveInfo.Name);
                //    Console.WriteLine("File type: {0}", driveInfo.DriveType);
                //    if (driveInfo.IsReady == true)
                //    {
                //        Console.WriteLine("Volume label: {0}", driveInfo.VolumeLabel);
                //        Console.WriteLine("File system: {0}", driveInfo.DriveFormat);
                //        Console.WriteLine("Available space to current user:{0,15} bytes", driveInfo.AvailableFreeSpace);
                //        Console.WriteLine("Total available space: {0,15} bytes", driveInfo.TotalFreeSpace);
                //        Console.WriteLine("Total size of drive: {0,15} bytes", driveInfo.TotalSize);
                //    }
                //}
                //Console.ReadKey();

                #endregion

                #region Folders and files

                //var directoryInfo = new DirectoryInfo(@"C:\Folder");
                //if (!directoryInfo.Exists)
                //{
                //    Directory.CreateDirectory(@"C:\Folder");
                //}
                //FileInfo[] files = directoryInfo.GetFiles();

                //using (FileStream fileStream = File.Create(@"C:\Folder\file1.txt"))
                //{
                //    string myValue = "File1Content";
                //    byte[] data = Encoding.UTF8.GetBytes(myValue);
                //    fileStream.Write(data);
                //}

                //using (StreamWriter streamWriter = File.CreateText(@"C:\Folder\file2.txt"))
                //{
                //    string myValue = "File2Content";
                //    streamWriter.Write(myValue);
                //}

                //files = directoryInfo.GetFiles();

                //foreach (FileInfo file in files)
                //{
                //    Console.WriteLine("File name {0}", file.Name);
                //    Console.WriteLine("File extension: {0}", file.Extension);

                //    using (FileStream fileStream = file.Open(FileMode.Open))
                //    {
                //        byte[] bytesContent = new byte[fileStream.Length];
                //        fileStream.Read(bytesContent);
                //        string content = Encoding.UTF8.GetString(bytesContent);
                //        Console.WriteLine(content);
                //    }

                //    using (StreamReader streamReader = file.OpenText())
                //    {
                //        string content = streamReader.ReadToEnd();
                //        Console.WriteLine(content);
                //    }

                //    file.Delete();
                //}

                //directoryInfo.Delete();

                #endregion
            }

            // Serialization and deserialization
            {
                #region XML

                //Product product = new Product
                //{
                //    Name = "Some product",
                //    Amount = 9,
                //};

                //XmlSerializer xmlSerializer = new XmlSerializer(typeof(Product));

                //Directory.CreateDirectory(@"C:\Folder");
                //using (FileStream file = File.Create(@"C:\Folder\ProductToXml.txt"))
                //{
                //    xmlSerializer.Serialize(file, product);
                //}

                //using (FileStream fs = new FileStream(@"C:\Folder\ProductToXml.txt", FileMode.Open))
                //{
                //    Product prod = xmlSerializer.Deserialize(fs) as Product;
                //    Console.WriteLine($"{prod.Name} {prod.Amount}");
                //}
                //Console.ReadKey();

                #endregion

                #region JSON

                //Product product = new Product
                //{
                //    Name = "Some product",
                //    Amount = 9,
                //};

                //DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Product));

                //Directory.CreateDirectory(@"C:\Folder");
                //using (FileStream fs = File.Create(@"C:\Folder\ProductToJson.txt"))
                //{
                //    jsonSerializer.WriteObject(fs, product);
                //}

                //using (FileStream fs = new FileStream(@"C:\Folder\ProductToJson.txt", FileMode.Open))
                //{
                //    Product prod = jsonSerializer.ReadObject(fs) as Product;
                //    Console.WriteLine($"{prod.Name} {prod.Amount}");
                //}
                //Console.ReadKey();

                #endregion

                #region Advanced XML

                //List<Order> orders = new List<Order>
                //{
                //    new Order
                //    {
                //        Products = new List<Product>
                //        {
                //            new Product { Amount = 6, Name = "Camera" },
                //            new Product { Amount = 1, Name = "Pizza" },
                //        }
                //},
                //    new Order
                //    {
                //        Products = new List<Product>
                //        {
                //            new Product { Amount = 3, Name = "TvGame" },
                //            new Product { Amount = 5, Name = "TV" },
                //        }
                //    },
                //};


                //XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Order>));

                //Directory.CreateDirectory(@"C:\Folder");
                //using (FileStream file = File.Create(@"C:\Folder\ListToXml.txt"))
                //{
                //    xmlSerializer.Serialize(file, orders);
                //}

                //using (FileStream fs = new FileStream(@"C:\Folder\ListToXml.txt", FileMode.Open))
                //{
                //    List<Order> ord = xmlSerializer.Deserialize(fs) as List<Order>;
                //}
                //Console.ReadKey();

                #endregion
            }
        }
    }
}
