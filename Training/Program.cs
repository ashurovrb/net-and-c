﻿using System;

namespace Training
{
    class Program
    {
        static void Main(string[] args)
        {
            #region commentaries

            // single line

            /* multi 
             * line
             * commentary */

            #endregion

            #region operators

            //// Assignment operator
            //string inportantPhrase = "Hello World!";
            //Console.WriteLine(inportantPhrase);
            //Console.ReadKey();

            //// Logical negation
            //bool x = true;
            //bool y = !x;
            //Console.WriteLine($"intital value of x: {x}, was inverted to {y}");
            //Console.ReadKey();

            //// Multiplicative and Additive operators
            //int result = 100 + 2 * 10 / 5;
            //Console.WriteLine($"100 + 2 * 10 / 5 = {result}");
            //Console.ReadKey();

            //// Relational operators
            //int a = 5;
            //bool b = a > 100;
            //bool c = a <= 5;
            //Console.WriteLine($"5 > 100 is {b}");
            //Console.WriteLine($"5 <= 5 is {c}");
            //Console.ReadKey();

            //// Type operators
            //int m = 5;
            //bool isInt = m is int;
            //bool isString = m is string;
            //Console.WriteLine($"m is int: {isInt}");
            //Console.WriteLine($"m is string: {isString}");
            //Console.ReadKey();

            //// Equality operators
            //string hello = "hello";
            //string world = "world";
            //bool isEqual = hello == world;
            //bool isNotEqual = hello != world;
            //Console.WriteLine($"hello is equal to world {isEqual}");
            //Console.WriteLine($"hello is not equal to world {isNotEqual}");
            //Console.ReadKey();

            //// Logical operators
            //bool positive = true;
            //bool negative = false;
            //bool and = positive & negative;
            //bool or = positive | negative;
            //// "short-circuiting" versions
            //bool andShort = positive && negative;
            //bool orShort = positive || negative;
            //// true if only one value is true
            //bool xor = positive ^ negative;
            //Console.WriteLine($"and : {and}");
            //Console.WriteLine($"or : {or}");
            //Console.WriteLine($"andShort : {andShort}");
            //Console.WriteLine($"orShort : {orShort}");
            //Console.WriteLine($"xor : {xor}");
            //Console.ReadKey();

            //// Conditional operator (ternary operator)
            //bool logicalVariable = true;
            //string res = logicalVariable ? "positive" : "negative";
            //Console.WriteLine($"res : {res}");
            //Console.ReadKey();

            #endregion

            #region declaration and expression statements

            //// Variable declaration statements.
            //double area;

            //// Constant declaration statement and value assigning
            //const double pi = 3.14159;

            //// An expression is a sequence of one or more operands and zero or more operators that can be evaluated to a single value, object, method, or namespace.

            //// Expression statements that calculate a value must store the value in a variable
            ////2 * 2;

            //// Expression statement (assignment).
            //area = 2 * 2;

            //// Expression statement (method invocation).
            //Console.WriteLine(area * pi);
            //Console.ReadKey();

            #endregion

            #region selection statements

            //bool isCSharpIsGood = true;

            //if(isCSharpIsGood)
            //{
            //    Console.WriteLine("C# is good");
            //}
            //else
            //{
            //    Console.WriteLine("C# is bad");
            //}
            //Console.ReadKey();

            //string somevalue = Console.ReadLine();

            //if (somevalue == "hello")
            //{
            //    Console.WriteLine("Hi User!");
            //}
            //else if (somevalue == "goodbye")
            //{
            //    Console.WriteLine("Well good buy User!");
            //}
            //else Console.WriteLine($"I dont know what to answer on message: {somevalue}");
            //Console.ReadKey();

            //string somevalue = Console.ReadLine();

            //switch (somevalue)
            //{
            //    case "hello":
            //        Console.WriteLine("Hi User!");
            //        break;
            //    case "goodbye":
            //        Console.WriteLine("Well good buy User!");
            //        break;
            //    default:
            //        Console.WriteLine($"I dont know what to answer on message: {somevalue}");
            //        break;
            //}
            //Console.ReadKey();

            #endregion

            #region iteration statements 

            //int n = 0;
            //while (n < 5)
            //{
            //    Console.WriteLine("current value is " + n);
            //    n++;
            //}
            //Console.ReadKey();

            //int m = 0;
            //do
            //{
            //    Console.WriteLine("current value is " + m);
            //    m++;
            //} while (m < 5);
            //Console.ReadKey();

            //for (int variable = 0; variable < 5; variable++)
            //{
            //    Console.WriteLine("current value is " + variable);
            //}
            //Console.ReadKey();

            //int i;
            //int j = 10;
            //for (i = 0, Console.WriteLine($"Start with values: i={i}, j={j}"); i < j; i++, j--, Console.WriteLine($"Next step with values: i={i}, j={j}"))
            //{
            //    Console.WriteLine($"{i} is lesser than {j}");
            //}
            //Console.WriteLine($"{i} is not lesser than {j} so iteration is finished");
            //Console.ReadKey();



            //var fibNumbers = new int[] { 0, 1, 1, 2, 3, 5, 8, 13 };
            //int elementIndex = 0;
            //foreach (int element in fibNumbers)
            //{
            //    elementIndex++;
            //    Console.WriteLine($"Element #{elementIndex}: {element}");
            //}
            //Console.ReadKey();

            #endregion

            #region conditional statement

            //for (int i = 1; i <= 10; i++)
            //{
            //    if (i == 5)
            //    {
            //        break;
            //    }
            //    Console.WriteLine(i);
            //}
            //Console.ReadKey();

            //for (int i = 1; i <= 10; i++)
            //{
            //    if (i == 5)
            //    {
            //        continue;
            //    }
            //    Console.WriteLine(i);
            //}
            //Console.ReadKey();


            //    bool isCSharpIsGood = false;
            //    //bool isCSharpIsGood = true;

            //    if (isCSharpIsGood)
            //    {
            //        goto PositiveAction;
            //    }
            //    else
            //    {
            //        goto NegativeAction;
            //    }

            //PositiveAction:
            //    Console.WriteLine("C# is good");
            //NegativeAction:
            //    Console.WriteLine("C# is good");

            //    Console.ReadKey();

            #endregion
        }
    }
}
